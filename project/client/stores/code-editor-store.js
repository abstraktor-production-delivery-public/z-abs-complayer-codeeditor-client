
'use strict';

import { DataActionCodeEditorWorkspaceGet, DataActionCodeEditorWorkspaceNew, DataActionCodeEditorWorkspaceSearch } from '../actions/data-action-code-editor-workspace';
import { DataActionCodeEditorProjectUpdate, DataActionCodeEditorProjectToggle } from '../actions/data-action-code-editor-project';
import { DataActionCodeEditorFileGet, DataActionCodeEditorFileNew, DataActionCodeEditorFileRename, DataActionCodeEditorFileUpdate, DataActionCodeEditorFileDelete } from '../actions/data-action-code-editor-file';
import { DataActionCodeEditorFolderNew, DataActionCodeEditorFolderUpdate, DataActionCodeEditorFolderDelete } from '../actions/data-action-code-editor-folder';
import { ActionModalDialogResult } from 'z-abs-complayer-modaldialog-client/client/actions/action-modal-dialog';
import StoreBaseRealtime from 'z-abs-corelayer-client/client/store/store-base-realtime';
import GuidGenerator from 'z-abs-corelayer-cs/clientServer/guid-generator';
import Project from 'z-abs-corelayer-cs/clientServer/project';


class CodeEditorStore extends StoreBaseRealtime {
  static showWorkspace = false;
  
  constructor() {
    super({
      project: new Project(),
      projects: new Map(),
      selectedProjectId: '',
      files: new Map(),
      current: {
        file: null,
        folder: null,
        type: '',
        query: '',
        queries: new Map()
      },
      build: {
        errors: []
      },
      persistentData: {
        darkMode: false,
        emptyLine: true
      },
      eol: '\n'
    });
  }
  
  _getAppName() {
    return this.state.current.query?.workspace ? this.state.current.query.workspace : window.abstractorReleaseData.appName;
  }
  
  _getWorkspaceName() {
    return this.state.current.query?.workspace ? this.state.current.query.workspace : window.abstractorReleaseData.appName;
  }
  
  onInit() {
    const query = this._createQuery(location.url, location.search);
    const appName = query?.workspace ? query.workspace : window.abstractorReleaseData.appName;
    const workspaceName = query?.workspace ? query.workspace : window.abstractorReleaseData.appName;
    this.sendDataAction(new DataActionCodeEditorWorkspaceGet(appName, workspaceName, true));
  }
  
  onActionCodeEditorWorkspaceGet(action) {
    const appName = action.appName ? action.appName : this._getAppName();
    const workspaceName = action.workspaceName ? action.workspaceName : this._getWorkspaceName();
    this.sendDataAction(new DataActionCodeEditorWorkspaceGet(appName, workspaceName, false));
  }
  
  onActionCodeEditorPersistentDataUpdate(action) {
    const persistentData = this.deepCopy(this.state.persistentData);
    const parent = action.parentName ? Reflect.get(persistentData, action.parentName) : persistentData;
    Reflect.set(parent, action.name, action.value);
    this.updateState({persistentData: {$set: parent}});
  }
  
  onActionCodeEditorWorkspaceNew(action) {
    this.sendDataAction(new DataActionCodeEditorWorkspaceNew(action.appName, action.workspaceName));
  }
  
  onActionCodeEditorWorkspaceSearch(action) {
    this.sendDataAction(new DataActionCodeEditorWorkspaceSearch(action));
  }
  
  onActionCodeEditorWorkspaceSearchSet(action) {
    const data = this.state.files.get(action.key);
    this.updateState({current: {file: {$set: data}}});
    this.updateState({current: {type: {$set: CodeEditorStore.FILE_TYPE_SEARCH}}});
  }
  
  onActionCodeEditorFileGet(action) {
    const file = this.state.files.get(action.key);
    if(!file) {
      const project = this._getProject(action.projectId);
      this.sendDataAction(new DataActionCodeEditorFileGet(`${action.path}/${action.title}`, action.projectId, action.key, action.title, action.path, action.type, project.type, project.plugin, project.workspaceName));
    }
    else {
      this._updateProject(action.projectId, (project, app) => {
        project.select(action.key, true);
      });
      this._setSelectedProject(action.projectId);
      this._setCurrentFile(file);
    }
  }
  
  onActionCodeEditorFileNew(action) {
    const project = this._getProject(action.projectId);
    this.sendDataAction(new DataActionCodeEditorFileNew(action, project.type, project.plugin, project.workspaceName));
  }
  
  onActionCodeEditorFileAdd(action) {
    let node = null;
    let projectType = null;
    let projectPlugin = null;
    let projectWorkspaceName = null;
    this._updateProject(action.projectId, (project, app) => {
      node = project.addFile(action.title, action.path, action.type);
      project.sortParent(node);
      if(!app) {
        projectType = project.type;
        projectPlugin = project.plugin;
      }
      else {
        projectWorkspaceName = project.workspaceName;
      }
    });
    this.sendDataAction(new DataActionCodeEditorFileGet(`${node.data.path}/${node.title}`, action.projectId, node.key, node.title, node.data.path, node.data.type, projectType, projectPlugin, projectWorkspaceName));
  }
  
  onActionCodeEditorFileRename(action) {
    const project = this._getProject(action.projectId);
    this.sendDataAction(new DataActionCodeEditorFileRename(`${action.path}/${action.title}`, `${action.path}/${action.newTitle}`, project.type, project.plugin, action, project.workspaceName));
  }
  
  onActionCodeEditorFileUpdate(action) {
    const file = this.state.files.get(action.key);
    const project = this._getProject(file.projectId);
    this.sendDataAction(new DataActionCodeEditorFileUpdate(action.key, file.title, file.path, file.content, project.type, project.plugin, project.workspaceName));
  }
  
  onActionCodeEditorFileUpdateAll(action) {
    action.keys.forEach((key) => {
      const file = this.state.files.get(key);
      const project = this._getProject(file.projectId);
      this.sendDataAction(new DataActionCodeEditorFileUpdate(key, file.title, file.path, file.content, project.type, project.plugin, project.workspaceName));
    });
  }
    
  onActionCodeEditorFileEdit(action) {
    const file = this.state.files.get(action.key);
    this.updateState({current: {file: { content: {$set: action.content}}}});
    this.updateState({current: {file: { codeChanged: {$set: action.content !== file.contentOriginal}}}});
    this.updateState({files: (files) => {
      files.set(action.key, this.state.current.file);
    }});
  }
  
  onActionCodeEditorFileClose(action) {
    const file = this.state.files.get(action.key);
    const nextFile = this._getNextFile(file);
    this.updateState({files: (files) => {
      files.delete(action.key);
    }});
    this._setCurrentFile(nextFile);
  }
  
  onActionCodeEditorFileRemove(action) {
    const file = this.state.files.get(action.key) || this.state.current.file;
    const nextFile = this._getNextFile(file);
    this.updateState({files: (files) => {
      files.delete(action.key);
    }});
    this._updateProject(action.projectId, (project, app) => {
      project.removeNode(file.path, file.key);
    });
    if(nextFile) {
      this._setCurrentFile(nextFile);
    }
    else {
      const node = this._getNode(file.path);
      const folder = this._createFolder(node.projectId, node.node);
      this._setCurrentFolder(folder);
    }
  }
  
  onActionCodeEditorFileDelete(action) {
    const file = this.state.files.get(action.key);
    const project = this._getProject(action.projectId);
    this.sendDataAction(new DataActionCodeEditorFileDelete(`${file.path}/${file.title}`, project.type, project.plugin, action, project.workspaceName));
  }
  
  onActionCodeEditorFolderGet(action) {
    const project = this._getProject(action.projectId);
    const node = project.findNode(`${action.data.path}/${action.title}` );
    const folder = this._createFolder(action.projectId, node);
    this._updateProject(action.projectId, (project, app) => {
      project.select(action.key, true);
    });
    this._setSelectedProject(action.projectId);
    this._setCurrentFolder(folder);
  }
  
  onActionCodeEditorFolderNew(action) {
    const project = this._getProject(action.projectId);
    this.sendDataAction(new DataActionCodeEditorFolderNew(action, project.type, project.plugin, project.workspaceName));
  }
   
  onActionCodeEditorFolderAdd(action) {
    let node;
    this._updateProject(action.projectId, (project, app) => {
      node = project.addFolder(action.title, action.data);
      project.sortParent(node);
    });
  }
  
  onActionCodeEditorFolderUpdate(action) {
    const project = this._getProject(action.projectId);
    this.sendDataAction(new DataActionCodeEditorFolderUpdate(`${action.data.path}/${action.title}`, `${action.data.path}/${action.newTitle}`, action, project.type, project.plugin, project.workspaceName));
  }
  
  onActionCodeEditorFolderRemove(action) {
    const node = this._getNode(`${action.data.path}/${action.title}`);
    const children = this.state.project.getAllFileChildren(node.node);
    
    children.forEach((child) => {
      const file = this.state.files.get(child.key);
      if(undefined !== file) {
        this.onActionCodeEditorFileClose({ key: child.key});
      }
    });
    const nodeParent = this._getNode(node.node.data.path);
    const folder = this._createFolder(nodeParent.projectId, nodeParent.node);
    this._updateProject(action.projectId, (project, app) => {
      project.removeNode(action.data.path, action.key);
    });
    this._setCurrentFolder(folder);
  }
  
  onActionCodeEditorFolderDelete(action) {
    const project = this._getProject(action.projectId);
    this.sendDataAction(new DataActionCodeEditorFolderDelete(action, project.type, project.plugin, project.workspaceName));
  }
  
  onActionCodeEditorFileMove(action) {
    let found = false;
    let foundFromKey = false;
    let foundToKey = false;
    let newMap = new Map();
    let fromFile = null;
    this.state.files.forEach((file, key) => {
      if(!found) {
        if(action.fromKey === key) {
          foundFromKey = true;
          found = true;
        }
        else if(action.toKey === key) {
          fromFile = this.state.files.get(action.fromKey);
          newMap.set(action.fromKey, fromFile);
          newMap.set(action.toKey, file);
          foundToKey = true;
          found = true;
        }
        else {
          newMap.set(key, file);
        }
      }
      else {
        if(foundFromKey) {
          if(action.toKey === key) {
            newMap.set(action.toKey, file);
            fromFile = this.state.files.get(action.fromKey);
            newMap.set(action.fromKey, fromFile);
          }
          else if(action.fromKey !== key) {
            newMap.set(key, file);
          }
        }
        else if(foundToKey) {
           if(action.fromKey !== key) {
            newMap.set(key, file);
          }
        }
      }
    });
    this.updateState({files: {$set: newMap}});
    this._setCurrentFile(fromFile);
  }
  
  onActionCodeEditorProjectUpdateAll(action) {
    action.projectIds.forEach((projectId) => {
      const project = this._getProject(projectId);
      if(!project.type) {
        this.sendDataAction(new DataActionCodeEditorProjectUpdate(project.projectId, project.source, this._getAppName()));
      }
      else {
        this.sendDataAction(new DataActionCodeEditorProjectUpdate(project.projectId, project.source, project.source[0].title, project.type, project.plugin));
      }
    });
  }
  
  onActionCodeEditorProjectToggle(action) {
    let isApp = true;
    this._updateProject(action.projectId, (project, app) => {
      isApp = app;
      project.toggle(action.key, action.expanded);
    });
    const project = this._getProject(action.projectId);
    if(isApp) {
      this.sendDataAction(new DataActionCodeEditorProjectToggle(action, this.state.project.source, this._getAppName()));
    }
    else {
      this.sendDataAction(new DataActionCodeEditorProjectToggle(action, project.source, project.source[0].title, project.type));
    }
  }
  
  onActionActorEditorBuild(action) {
    this.sendDataAction(new DataActionCodeEditorBuild(this.state.current.file.content, this.state.current.file.path, this.state.current.file.title));
  }
  
  _loadNode(currentUrl) {
    const node = this._getNode(currentUrl);
    if(node) {
      if(!node.node.folder) {
        const project = this._getProject(node.projectId);
        this.sendDataAction(new DataActionCodeEditorFileGet(currentUrl, node.projectId, node.node.key, node.node.title, node.node.data.path, node.node.data.type, project.type, project.plugin, project.workspaceName));
      }
      else {
        const folder = this._createFolder(node.projectId, node.node);
        this._updateProject(node.projectId, (project, app) => {
          project.select(folder.key, true);
        });
        this._setSelectedProject(node.projectId);
        this._setCurrentFolder(folder);
      }
    }
  }
  
  onDataActionCodeEditorWorkspaceGet(action) {
    const response = action.getResponse();
    if(response.isSuccess()) {
      if(Array.isArray(response.data)) {
        this.updateState({project: (project) => {
          project.set(response.data[0], null, action.workspaceName);
        }});
        this.updateState({eol: {$set: response.data[2]}});
        this.updateState({projects: (projects) => {
          projects.clear();
          const projectsExternal = response.data[1];
          projectsExternal.sort((a, b) => {
            if(a.source[0].title < b.source[0].title) {
              return -1;  
            }
            else if(a.source[0].title > b.source[0].title) {
              return 1;
            }
            else {
              return 0;
            }
          });
          this.updateState({files: (files) => {
            if(0 !== files.size) {
              files.clear();
            }
          }});
          for(let i = 0; i < projectsExternal.length; ++i) {
            const projectData = projectsExternal[i];
            const project = new Project(projectData.source, projectData.type, projectData.plugin);
            projects.set(project.projectId, project);
          }
        }});
      }
      const query = this._createQuery(location.url, location.search);
      const currentUrl = location.search ? `./${query.files}` : `.${location.pathname.substring(12)}`;
      if(action.open) {
        if(location.search) {
          // TODO: LINE
          this.updateState({current: {query: {$set: query}}});
          if(query.files) {
            this._loadNode(currentUrl);
          }
        }
        else {
          this.updateState({current: {query: {$set: {workspace: action.appName}}}});
          this._loadNode(currentUrl);
        }
      }
      else {
        if(query?.workspace !== action.appName) {
          this.updateState({current: {query: {$set: {workspace: action.appName}}}});
          this._setCurrentFile(null);
        }
        else {
          
        }
      }
    }
        /*if(query?.workspace !== action.appName) {
          this.updateState({current: {query: {$set: {workspace: action.appName}}}}); 
        }
        else {
          const node = this._getNode(currentUrl);
          if(node) {
            if(!node.node.folder) {
              const project = this._getProject(node.projectId);
              this.sendDataAction(new DataActionCodeEditorFileGet(currentUrl, node.projectId, node.node.key, node.node.title, node.node.data.path, node.node.data.type, project.type, project.plugin, project.workspaceName));
            }
            else {
              const folder = this._createFolder(node.projectId, node.node);
              this._updateProject(node.projectId, (project, app) => {
                project.select(folder.key, true);
              });
              this._setSelectedProject(node.projectId);
              this._setCurrentFolder(folder);
            }
          }
        }
      }
      else {
        
      }*/
      /*if(location.pathname.startsWith('/code-editor')) {
        //if(location.search) {
          console.log(location.search);
          const query = this._createQuery(location.url, location.search);
          //this.updateState({current: {query: {$set: query}}});
          const currentUrl = location.search ? `./${query.files}` : `.${location.pathname.substring(12)}`;
        //}
        //else {
        //  currentUrl = `.${location.pathname.substring(12)}`; // /code-editor
        //  this.updateState({current: {query: {$set: {workspace: action.appName}}}}); 
        //}
        //if(query?.workspace !== action.appName) {
          this.updateState({current: {query: {$set: {workspace: action.appName}}}}); 
        //}
        console.log('***', currentUrl, action.appName);
        const node = this._getNode(currentUrl);
        if(node) {
          if(!node.node.folder) {
            const project = this._getProject(node.projectId);
            this.sendDataAction(new DataActionCodeEditorFileGet(currentUrl, node.projectId, node.node.key, node.node.title, node.node.data.path, node.node.data.type, project.type, project.plugin, project.workspaceName));
          }
          else {
            const folder = this._createFolder(node.projectId, node.node);
            this._updateProject(node.projectId, (project, app) => {
              project.select(folder.key, true);
            });
            this._setSelectedProject(node.projectId);
            this._setCurrentFolder(folder);
          }
        }
      }
    }*/
  }
  
  onDataActionCodeEditorWorkspaceNew(action) {
    const response = action.getResponse();
    if(response.isSuccess()) {
      //this.updateState({current: {workspace: {$set: new Map(response.data)}}});
    }
  }
  
  onDataActionCodeEditorWorkspaceSearch(action) {
    const response = action.getResponse();
    if(response.isSuccess()) {
      const data = {
        key: GuidGenerator.create(),
        tabType: CodeEditorStore.FILE_TYPE_SEARCH,
        titleHeading: 'search',
        titleHeadingClassName: 'editor_search_header',
        title: ` ${action.search.length <= 23 ? action.search : action.search.substring(0, 20) + '...'}`,
        search: action.search,
        data: response.data
      };
      this.updateState({files: (files) => {
        files.set(data.key, data); 
      }});
      this.updateState({current: {file: {$set: data}}});
      this.updateState({current: {type: {$set: CodeEditorStore.FILE_TYPE_SEARCH}}});
    }
  }
  
  onDataActionCodeEditorFileGet(action) {
    const response = action.getResponse();
    if(response.isSuccess()) {
      //if(this.state.current.file?.key !== action.key) {
        const file = {
          projectId: action.projectId,
          key: action.key,
          title: action.title,
          path: action.path,
          type: action.type,
          plugin: action.plugin,
          workspaceName: action.workspaceName,
          content: response.data,
          contentOriginal: response.data,
          codeChanged: false,
          codeBuilt: false
        };
        this.updateState({files: (files) => { files.set(action.key, file); }});
        this._updateProject(action.projectId, (project, app) => {
          const node = project.findNode(`${action.path}/${action.title}`);
          project.select(action.key, true);
          node.data.valid = true;
        });
        this._setSelectedProject(action.projectId);
        this._setCurrentFile(file);
      //}
    }
    else {
      this._updateProject(action.projectId, (project, app) => {
        const node = project.findNode(`${action.path}/${action.title}`);
        node.data.valid = false;
        project.select(action.key, true);
      });
      this._setSelectedProject(action.projectId);
      const file = {
        projectId: action.projectId,
        key: action.key,
        title: action.title,
        path: action.path,
        type: action.type,
        plugin: action.plugin,
        workspaceName: action.workspaceName,
        codeChanged: false,
        codeBuilt: false
      };
      this._setCurrentFile(file);
    }
  }
  
  onDataActionCodeEditorFileNew(action) {
    const response = action.getResponse();
    if(response.isSuccess()) {
      let node;
      let projectType;
      let projectPlugin;
      let projectWorkspaceName;
      this._updateProject(action.projectId, (project, app) => {
        node = project.addFile(response.data, action.path, action.type);
        project.sortParent(node);
        if(!app) {
          projectType = project.type;
          projectPlugin= project.plugin;
        }
        else {
          projectWorkspaceName = project.workspaceName;
        }
      });
      this.sendDataAction(new DataActionCodeEditorFileGet(`${node.data.path}/${node.title}`, action.projectId, node.key, node.title, node.data.path, node.data.type, projectType, projectPlugin, projectWorkspaceName));
    }
  }
  
  onDataActionCodeEditorFileRename(action) {
    const response = action.getResponse();
    if(response.isSuccess()) {
      let node = this._getNode(`${action.path}/${action.title}`);
      this._updateProject(action.projectId, (project, app) => {
        project.renameFile(node.node, action.newTitle);
      });
      let file = this.state.files.get(node.node.key);
      const updatedFile = this.shallowCopy(file);
      updatedFile.title = action.newTitle;
      this.updateState({files: (files) => {
        file = files.set(node.node.key, updatedFile);
      }});
      this._setCurrentFile(updatedFile);
    }
  }
  
  onDataActionCodeEditorFileUpdate(action) {
    const response = action.getResponse();
    if(response.isSuccess()) {
      let file = this.state.files.get(action.key);
      if(undefined !== file) {
        let updatedFile = this.shallowCopy(file);
        updatedFile.codeChanged = false;
        updatedFile.contentOriginal = action.content;
        this.updateState({files: (files) => {
          files.set(action.key, updatedFile);
        }});
        if(this.state.current.file.key === action.key) {
          this.updateState({current: {file: {$set: updatedFile}}});
        }        
      }
    }
  }
  
  onDataActionCodeEditorFileDelete(action) {
    const response = action.getResponse();
    if(response.isSuccess()) {
      this.onActionCodeEditorFileRemove(action);
    }
  }
  
  onDataActionCodeEditorFolderNew(action) {
    const response = action.getResponse();
    if(response.isSuccess()) {
      let node;
      this._updateProject(action.projectId, (project, app) => {
        node = project.addFolder(action.title, action.data);
        project.sortParent(node);
      });
    }
  }
  
  onDataActionCodeEditorFolderUpdate(action) {
    const response = action.getResponse();
    if(response.isSuccess()) {
      let node = this._getNode(`${action.data.path}/${action.title}`);
      const children = this.state.project.getAllFileChildren(node.node);
      this._updateProject(action.projectId, (project, app) => {
        project.renamePathRecursive(node.node, action.newTitle);
      });
      node = this._getNode(`${action.data.path}/${action.newTitle}`);
      let folder = this._createFolder(node.projectId, node.node);
      this._setCurrentFolder(folder);
      children.forEach((child) => {
        let file;
        this.updateState({files: (files) => {
          file = files.get(child.key);
          if(undefined !== file) {
            file = this.shallowCopy(file);
            file.path = child.data.path;
            files.set(child.key, file);
          }
        }});
        /*if(this.state.current.file && this.state.current.file.key === child.key) {
          this.updateState({current: {file: {$set: file}}});
        }*/
      });
    }
  }
  
  onDataActionCodeEditorFolderDelete(action) {
    const response = action.getResponse();
    if(response.isSuccess()) {
      this.onActionCodeEditorFolderRemove(action);
    }
  }
    
  onDataActionCodeEditorProjectUpdate(action) {
    const response = action.getResponse();
    if(response.isSuccess()) {
      this._updateProject(action.projectId, (project, app) => {
        project.setSaved();
      });
    }
  }
  
  onDataActionCodeEditorProjectToggle(action) {
    const response = action.getResponse();
    if(response.isSuccess()) {
    }
  }
  
  _getNextFile(file) {
    let next;
    let mapIter = this.state.files.values();
    let iter = mapIter.next();
    while(!iter.done) {
      if(iter.value.key === file.key) {
        if(undefined !== next) {
          return next;
        }
        else {
          return mapIter.next().value;
        }  
      }
      next = iter.value;
      iter = mapIter.next();
    }
  }
  
  _getProject(projectId) {
    if(projectId === this.state.project.projectId) {
      return this.state.project;
    }
    else {
      return this.state.projects.get(projectId);
    }
  }
    
  _updateProject(projectId, cb) {
    if(projectId === this.state.project.projectId) {
      this.updateState({project: (project) => cb(project, true)});
    }
    else {
      this.updateState({projects: (projects) => {
        const project = projects.get(projectId);
        if(project) {
          const clonedProject = project.clone();
          cb(clonedProject, false);
          projects.set(projectId, clonedProject); // MUST update projectObject to trigger React.
        }
      }});
    }
  }
  
  _setSelectedProject(projectId) {
    if(projectId !== this.state.selectedProjectId) {
      if(this.state.selectedProjectId) {
        this._updateProject(this.state.selectedProjectId, (project, app) => {
          project.unSelect();
        });
      }
      this.updateState({selectedProjectId: {$set: projectId}});
    }
  }
  
  _getNode(file) {
    const node = this.state.project.findNode(file);
    if(undefined !== node) {
      return {
        projectId: this.state.project.projectId,
        node: node
      };
    }
    else {
      for(let projectItem of this.state.projects) {
        const project = projectItem[1];
        const node = project.findNode(file);
        if(undefined !== node) {
          return {
            projectId: project.projectId,
            node: node
          };
        }
      }
    }
  }
  
  _createFolder(projectId, node) {
    return {
      projectId: projectId,
      key: node.key,
      title: node.title,
      data: node.data
    };
  }
  
  _setCurrentFile(file) {
    this.updateState({current: {file: {$set: file}}});
    this.updateState({current: {folder: {$set: null}}});
    if(file) {
      this.updateState({current: {type: {$set: CodeEditorStore.FILE}}});
      this.history(this._generateQuery(file.path, file.title), {replace: true, noSlash:CodeEditorStore.showWorkspace});
    }
    else {
      this.updateState({current: {type: {$set: ''}}});
      this.history(this._generateQuery(null, null), {replace: true, noSlash:CodeEditorStore.showWorkspace});
    }
  }
  
  _setCurrentFolder(folder) {
    this.updateState({current: {folder: {$set: folder}}});
    this.updateState({current: {type: {$set: CodeEditorStore.FOLDER}}});
    this.history(this._generateQuery(folder.data.path, folder.title), {replace: true, noSlash:CodeEditorStore.showWorkspace});
  }
  
  _createQuery(url, search) {
    const query = {};
    if(search && search.length > 1) {
      const searchs = search.substring(1).split('&');
      if(searchs && searchs.length >= 1) {
        searchs.forEach((searchPart) => {
          const queryParam = searchPart.split('=');
          if(2 === queryParam.length) {
            Reflect.set(query, queryParam[0], queryParam[1]);
          }
        });
      }
    }
    return query;
    /*if(query && query.line) {
      return {
        url: url,
        line: Number.parseInt(query.line),
        css: query.type ? query.type : undefined
      }
    }*/
  }
  
  _generateQuery(path, title) {
    //const url = `${path}/${title}`
    const query = this.state.current.query;
    /*if(query && query.url && query.url === url) {
      let queries = [];
      let resultQuery = '';
      if(query.line) {
        queries.push(`line=${query.line}`);
      }
      if(query.type) {
        queries.push(`type=${query.type}`);
      }
      if(0 !== queries.length) {
         resultQuery = `?${queries[0]}`;
      }
      for(let i = 1; i < queries.length; ++i) {
        resultQuery += `&${queries[i]}`;
      }
      return `${url.substring(1)}${resultQuery}`;
    }
    else {*/
    if(CodeEditorStore.showWorkspace) {
      if(path && title) {
        const url = `${path}/${title}`
        return `?workspace=${query.workspace}&files=${url.substring(2)}`;
      }
      else {
        return `?workspace=${query.workspace}`;
      }
    }
    else {
      const url = `${path}/${title}`
      return url.substring(1);
    }
  }
}

CodeEditorStore.FOLDER = 'folder';
CodeEditorStore.FILE = 'file';
CodeEditorStore.FILE_TYPE_SEARCH = 'search';
CodeEditorStore.FILE_INVALID = 'invalid_file';


module.exports = CodeEditorStore.export(CodeEditorStore);
