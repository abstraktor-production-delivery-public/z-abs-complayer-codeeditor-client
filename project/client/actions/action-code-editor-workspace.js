
'use strict';

import Action from 'z-abs-corelayer-client/client/communication/action';


export class ActionCodeEditorWorkspaceNew extends Action {
  constructor(appName, workspaceName) {
    super(appName, workspaceName);
  }
}

export class ActionCodeEditorWorkspaceGet extends Action {
  constructor(appName, workspaceName, open) {
    super(appName, workspaceName, open);
  }
}

export class ActionCodeEditorWorkspaceUpdate extends Action {
  constructor(projectId) {
    super(projectId);
  }
}

export class ActionCodeEditorWorkspaceSearch extends Action {
  constructor(search) {
    super(search);
  }
}

export class ActionCodeEditorWorkspaceSearchSet extends Action {
  constructor(key) {
    super(key);
  }
}
