
'use strict';

import { ActionCodeEditorPersistentDataAdd, ActionCodeEditorPersistentDataGet } from './action-code-editor-persistent-data';
import DataAction from 'z-abs-corelayer-client/client/communication/data-action';


export class DataActionCodeEditorPersistentDataAdd extends DataAction {
  constructor(key, valueObject) {
    super();
    this.addRequest(new ActionCodeEditorPersistentDataAdd(), key, valueObject);
  }
}

export class DataActionCodeEditorPersistentDataGet extends DataAction {
  constructor(key) {
    super();
    this.addRequest(new ActionCodeEditorPersistentDataGet(), key);
  }
}

export class DataActionCodeEditorPersistentDataUpdate extends DataAction {
  constructor(action, key, valueObject) {
    super();
    this.addRequest(action, key, valueObject);
  }
}
