
'use strict';

import { ActionCodeEditorWorkspaceNew, ActionCodeEditorWorkspaceGet, ActionCodeEditorWorkspaceUpdate } from './action-code-editor-workspace';
import DataAction from 'z-abs-corelayer-client/client/communication/data-action';


export class DataActionCodeEditorWorkspaceGet extends DataAction {
  constructor(appName, workspaceName, open) {
    super();
    this.addRequest(new ActionCodeEditorWorkspaceGet(appName, workspaceName, open), appName, workspaceName);
  }
}

export class DataActionCodeEditorWorkspaceNew extends DataAction {
  constructor(appName, appType) {
    super();
    this.addRequest(new ActionCodeEditorWorkspaceNew(appName, appType), appName, appType);
  }
}

export class DataActionCodeEditorWorkspaceSearch extends DataAction {
  constructor(action) {
    super();
    this.addRequest(action, action.search);
  }
}
