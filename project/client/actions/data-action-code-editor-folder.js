
'use strict';

import DataAction from 'z-abs-corelayer-client/client/communication/data-action';


export class DataActionCodeEditorFolderNew extends DataAction {
  constructor(action, projectType, plugin, workspaceName) {
    super();
    this.addRequest(action, `${action.data.path}/${action.title}`, projectType, plugin, workspaceName);
  }
}

export class DataActionCodeEditorFolderUpdate extends DataAction {
  constructor(oldPath, newPath, action, projectType, plugin, workspaceName) {
    super();
    this.addRequest(action, oldPath, newPath, projectType, plugin, workspaceName);
  }
}

export class DataActionCodeEditorFolderDelete extends DataAction {
  constructor(action, projectType, plugin, workspaceName) {
    super();
    this.addRequest(action, `${action.data.path}/${action.title}`, projectType, plugin, workspaceName);
  }
}
