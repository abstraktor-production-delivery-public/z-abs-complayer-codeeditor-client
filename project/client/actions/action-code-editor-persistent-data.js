
'use strict';

import Action from 'z-abs-corelayer-client/client/communication/action';


export class ActionCodeEditorPersistentDataAdd extends Action {
  constructor() {
    super();
  }
}

export class ActionCodeEditorPersistentDataGet extends Action {
  constructor() {
    super();
  }
}

export class ActionCodeEditorPersistentDataUpdate extends Action {
  constructor(parentName, name, value) {
    super(parentName, name, value);
  }
}
