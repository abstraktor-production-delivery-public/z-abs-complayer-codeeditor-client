
'use strict';

import Action from 'z-abs-corelayer-client/client/communication/action';


export class ActionCodeEditorFileGet extends Action {
  constructor(projectId, key, title, path, type, plugin, workspaceName) {
    super(projectId, key, title, path, type, plugin, workspaceName);
  }
}

export class ActionCodeEditorFileAdd extends Action {
  constructor(projectId, title, path, type, plugin) {
    super(projectId, title, path, type, plugin);
  }
}

export class ActionCodeEditorFileNew extends Action {
  constructor(projectId, path, name, type, templateName, plugin, workspaceName) {
    super(projectId, path, name, type, templateName, plugin, workspaceName);
  }
}

export class ActionCodeEditorFileRename extends Action {
  constructor(projectId, path, title, newTitle, workspaceName) {
    super(projectId, path, title, newTitle, workspaceName);
  }
}

export class ActionCodeEditorFileUpdate extends Action {
  constructor(key, workspaceName) {
    super(key, workspaceName);
  }
}

export class ActionCodeEditorFileUpdateAll extends Action {
  constructor(keys) {
    super(keys);
  }
}

export class ActionCodeEditorFileClose extends Action {
  constructor(key) {
    super(key);
  }
}

export class ActionCodeEditorFileRemove extends Action {
  constructor(projectId, key) {
    super(projectId, key);
  }
}

export class ActionCodeEditorFileDelete extends Action {
  constructor(projectId, key, plugin, workspaceName) {
    super(projectId, key, plugin, workspaceName);
  }
}

export class ActionCodeEditorFileEdit extends Action {
  constructor(projectId, key, content) {
    super(projectId, key, content);
  }
}

export class ActionCodeEditorFileMove extends Action {
  constructor(fromKey, toKey) {
    super(fromKey, toKey);
  }
}

