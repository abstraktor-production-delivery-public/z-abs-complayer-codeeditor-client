
'use strict';

import Action from 'z-abs-corelayer-client/client/communication/action';


export class ActionCodeEditorFolderGet extends Action {
  constructor(projectId, key, title, data, workspaceName) {
    super(projectId, key, title, data, workspaceName);
  }
}

export class ActionCodeEditorFolderNew extends Action {
  constructor(projectId, title, data, workspaceName) {
    super(projectId, title, data, workspaceName);
  }
}

export class ActionCodeEditorFolderAdd extends Action {
  constructor(projectId, title, data) {
    super(projectId, title, data);
  }
}

export class ActionCodeEditorFolderUpdate extends Action {
  constructor(projectId, title, newTitle, data, workspaceName) {
    super(projectId, title, newTitle, data, workspaceName);
  }
}

export class ActionCodeEditorFolderRemove extends Action {
  constructor(projectId, title, key, data) {
    super(projectId, title, key, data);
  }
}

export class ActionCodeEditorFolderDelete extends Action {
  constructor(projectId, title, key, data, workspaceName) {
    super(projectId, title, key, data, workspaceName);
  }
}
