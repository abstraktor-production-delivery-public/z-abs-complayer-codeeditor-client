
'use strict';

import Action from 'z-abs-corelayer-client/client/communication/action';


export class ActionCodeEditorProjectUpdate extends Action {
  constructor(projectId) {
    super(projectId);
  }
}

export class ActionCodeEditorProjectUpdateAll extends Action {
  constructor(projectIds) {
    super(projectIds);
  }
}

export class ActionCodeEditorProjectToggle extends Action {
  constructor(projectId, key, expanded, name) {
    super(projectId, key, expanded, name);
  }
}
