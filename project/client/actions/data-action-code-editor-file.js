
'use strict';

import { ActionCodeEditorFileGet, ActionCodeEditorFileUpdate } from './action-code-editor-file';
import DataAction from 'z-abs-corelayer-client/client/communication/data-action';


export class DataActionCodeEditorFileGet extends DataAction {
  constructor(file, projectId, key, title, path, type, projectType, plugin, workspaceName) {
    super();
    this.addRequest(new ActionCodeEditorFileGet(projectId, key, title, path, type, plugin), file, projectType, plugin, workspaceName);
  }
}

export class DataActionCodeEditorFileNew extends DataAction {
  constructor(action, projectType, plugin, workspaceName) {
    super();
    this.addRequest(action, action.path, action.name, action.type, action.templateName, projectType, plugin, workspaceName);
  }
}

export class DataActionCodeEditorFileRename extends DataAction {
  constructor(oldPath, newPath, projectType, plugin, action, workspaceName) {
    super();
    this.addRequest(action, oldPath, newPath, projectType, plugin, workspaceName);
  }
}

export class DataActionCodeEditorFileUpdate extends DataAction {
  constructor(key, title, path, content, projectType, plugin, workspaceName) {
    super();
    this.addRequest(new ActionCodeEditorFileUpdate(key), `${path}/${title}`, content, projectType, plugin, workspaceName);
  }
}

export class DataActionCodeEditorFileDelete extends DataAction {
  constructor(file, projectType, plugin, action, workspaceName) {
    super();
    this.addRequest(action, file, projectType, plugin, workspaceName);
  }
}
