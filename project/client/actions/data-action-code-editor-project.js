
'use strict';

import { ActionCodeEditorProjectUpdate } from './action-code-editor-project';
import DataAction from 'z-abs-corelayer-client/client/communication/data-action';


export class DataActionCodeEditorProjectUpdate extends DataAction {
  constructor(projectId, content, name, type, plugin) {
    super();
    this.addRequest(new ActionCodeEditorProjectUpdate(projectId), content, name, type, plugin);
  }
}

export class DataActionCodeEditorProjectToggle extends DataAction {
  constructor(action, content, name, type) {
    super();
    this.addRequest(action, content, name, type);
  }
}
