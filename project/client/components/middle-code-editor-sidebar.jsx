
'use strict';

import CodeEditorStore from '../stores/code-editor-store';
import { ActionCodeEditorFileGet } from '../actions/action-code-editor-file';
import { ActionCodeEditorFolderGet } from '../actions/action-code-editor-folder';
import { ActionCodeEditorProjectToggle } from '../actions/action-code-editor-project';
import FileIcons from 'z-abs-complayer-bootstrap-client/client/icons/file-icons';
import FolderIcons from 'z-abs-complayer-bootstrap-client/client/icons/folder-icons';
import Tree from 'z-abs-complayer-tree-client/client/react-components/tree';
import {RouterContext} from 'z-abs-complayer-router-client/client/react-component/router-context';
import ReactComponentStore from 'z-abs-corelayer-client/client/react-component/react-component-store';
import React from 'react';


export default class MiddleCodeEditorSidebar extends ReactComponentStore {
  constructor(props) {
    super(props, [CodeEditorStore]);
    this.boundMouseUp = this._boundMouseUp.bind(this);
    this.boundMouseMove = this._boundMouseMove.bind(this);
    this.sidebarNode = null;
    this.sidebarWidthMin = 180;
    this.sidebarWidthMax = 720;
  }
  
  shouldUpdate(nextProps, nextState) {
    return !this.shallowCompare(this.state.CodeEditorStore.project, nextState.CodeEditorStore.project)
      || !this.shallowCompare(this.state.CodeEditorStore.projects, nextState.CodeEditorStore.projects)
      || !this.shallowCompare(this.state.CodeEditorStore.current, nextState.CodeEditorStore.current)
      || !this.shallowCompare(this.state.CodeEditorStore.persistentData, nextState.CodeEditorStore.persistentData);
  }
  
  _boundMouseUp(e) {
    window.removeEventListener('mouseup', this.boundMouseUp, true);
    window.removeEventListener('mousemove', this.boundMouseMove, true);
    this.sidebarNode = null;
  }
  
  _boundMouseMove(e) {
    let width = e.clientX + 2;
    if(width < this.sidebarWidthMin) {
      width = this.sidebarWidthMin;
    }
    else if(width > this.sidebarWidthMax) {
      width = this.sidebarWidthMax;
    }
    this.sidebarNode.style.width = `${width}px`;
    this.sidebarNode.nextSibling.style.left = `${width}px`;
  }
  
  renderTree(project, key) {
    return (
      <Tree key={project.projectId} id={`code_editor_tree_code_${key}`} project={project} current={this.state.CodeEditorStore.current} folderIcons={FolderIcons} fileIcons={FileIcons} darkMode={this.state.CodeEditorStore.persistentData.darkMode}
        onClickFile={(key, title, path, type) => {
          this.dispatch(CodeEditorStore, new ActionCodeEditorFileGet(project.projectId, key, title, path, type, project.plugin));
        }}
        onClickFolder={(key, title, data) => {
          this.dispatch(CodeEditorStore, new ActionCodeEditorFolderGet(project.projectId, key, title, data));
        }}
        onToggleFolder={(key, expanded) => {
          this.dispatch(CodeEditorStore, new ActionCodeEditorProjectToggle(project.projectId, key, expanded));
        }}
      />
    );
  }
  
  renderTrees() {
    const trees = [];
    this.state.CodeEditorStore.projects.forEach((project, key) => {
      trees.push(this.renderTree(project, key));
    });
    return trees;
  }
  
  render() {
    const stylePanel = {
      height:'100%'
    };
    const darkMode = this.state.CodeEditorStore.persistentData.darkMode;
    const workspaceName = this.state.CodeEditorStore.current.query?.workspace;
    return (
      <div className={this.theme(darkMode, "sidebar", "middle_sidebar")}>
        <div className={this.theme(darkMode, " panel-default", "panel")} style={stylePanel}>
          <div className={this.theme(darkMode, "panel_body_sidebar")}>
            <div id="code_editor_tree_root">
              <Tree key={this.state.CodeEditorStore.project.projectId} id="code_editor_tree_code_project" project={this.state.CodeEditorStore.project} current={this.state.CodeEditorStore.current} folderIcons={FolderIcons} fileIcons={FileIcons} darkMode={this.state.CodeEditorStore.persistentData.darkMode}
                onClickFile={(key, title, path, type) => {
                  this.dispatch(CodeEditorStore, new ActionCodeEditorFileGet(this.state.CodeEditorStore.project.projectId, key, title, path, type, false, workspaceName));
                }}
                onClickFolder={(key, title, data) => {
                  this.dispatch(CodeEditorStore, new ActionCodeEditorFolderGet(this.state.CodeEditorStore.project.projectId, key, title, data, workspaceName));
                }}
                onToggleFolder={(key, expanded) => {
                  this.dispatch(CodeEditorStore, new ActionCodeEditorProjectToggle(this.state.CodeEditorStore.project.projectId, key, expanded));
                }}
              />
              {this.renderTrees()}
            </div>
          </div>
        </div>
        <div className={this.theme(darkMode, "sidebar_mover")}
          onMouseDown={(e) => {
            if(0 === e.button) {
              this.sidebarNode = e.target.parentNode;
              window.addEventListener('mouseup', this.boundMouseUp, true);
              window.addEventListener('mousemove', this.boundMouseMove, true);
            }
          }}
        >
        </div>
      </div>
    );
  }
}


MiddleCodeEditorSidebar.contextType = RouterContext;
