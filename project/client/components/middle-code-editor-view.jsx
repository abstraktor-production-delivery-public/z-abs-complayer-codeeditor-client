

'use strict';

import CodeEditorStore from '../stores/code-editor-store';
import { ActionCodeEditorWorkspaceGet } from '../actions/action-code-editor-workspace';
import { ActionCodeEditorFileGet, ActionCodeEditorFileEdit, ActionCodeEditorFileClose, ActionCodeEditorFileMove } from '../actions/action-code-editor-file';
import Tabs from 'z-abs-complayer-bootstrap-client/client/tabs';
import Tab from 'z-abs-complayer-bootstrap-client/client/tab';
import CodeMirrorEditor from 'z-abs-complayer-codemirror-client/client/code-mirror-editor';
import {RouterContext} from 'z-abs-complayer-router-client/client/react-component/router-context';
import ReactComponentStore from 'z-abs-corelayer-client/client/react-component/react-component-store';
import React from 'react';


export default class MiddleCodeEditorView extends ReactComponentStore {
  constructor(props) {
    super(props, [CodeEditorStore]);
    this.close = false;
    this.fileIcons = new Map();
    this.fileIcons.set('bld', '/abs-images/icon/build.svg');
    this.fileIcons.set('css', '/abs-images/icon/css.svg');
    this.fileIcons.set('js', '/abs-images/icon/js.svg');
    this.fileIcons.set('json', '/abs-images/icon/json.svg');
    this.fileIcons.set('jsx', '/abs-images/icon/jsx.svg');
    this.fileIcons.set('html', '/abs-images/icon/html.svg');
    this.fileIcons.set('md', '/abs-images/icon/md.svg');
    this.fileIcons.set('prj', '/abs-images/icon/project.svg');
  }
  
  shouldUpdate(nextProps, nextState) {
    return !this.shallowCompare(this.props._uriPath, nextProps._uriPath)
      || !this.shallowCompareObjectValues(this.props.location, nextProps.location)
      || !this.shallowCompareMapValues(this.state.CodeEditorStore.files, nextState.CodeEditorStore.files)
      || !this.shallowCompare(this.state.CodeEditorStore.current, nextState.CodeEditorStore.current)
      || !this.shallowCompare(this.state.CodeEditorStore.current.query, nextState.CodeEditorStore.current.query)
      || !this.shallowCompare(this.state.CodeEditorStore.persistentData, nextState.CodeEditorStore.persistentData);
  }
    
  renderTabFileHeading(fileType) {
    const filePath = this.fileIcons.get(fileType);
    if(filePath) {
      return (
        <img className="middle_code_editor_tab_icon" src={filePath} />
      );
    }
    else {
      return null;
    }
  }
  
  renderTabFile(file, key) {
    return (
      <Tab eventKey={key} key={key}
        title={
          <div>
            {this.renderTabFileHeading(file.type)}
            {file.title}
            <button type="button" className={this.theme(this.state.CodeEditorStore.persistentData.darkMode, 'middle_code_editor_close')} aria-label="Close"
              onClick={(e, a) => {
                this.close = true;
                this.dispatch(CodeEditorStore, new ActionCodeEditorFileClose(key));
              }}
            >
              ×
            </button>
          </div>
        }
        onMove={(fromKey, toKey) => {
          this.dispatch(CodeEditorStore, new ActionCodeEditorFileMove(fromKey, toKey));
        }}
      >
        <CodeMirrorEditor id={`code_editor_view_${key}`} projectId={file.projectId} docKey={key} code={file.content} options={this.getOptions(file.type)} darkMode={this.state.CodeEditorStore.persistentData.darkMode} emptyLine={this.state.CodeEditorStore.persistentData.emptyLine} currentType={this.state.CodeEditorStore.current.type} currentFile={this.state.CodeEditorStore.current.file} queries={this.state.CodeEditorStore.current.queries} eol={this.state.CodeEditorStore.eol}
          onChanged={(projectId, docKey, code) => {
            this.dispatch(CodeEditorStore, new ActionCodeEditorFileEdit(projectId, docKey, code));
          }}
          onFocusChange={(focused, docKey) => {
            if(focused) {
              const file = this.state.CodeEditorStore.files.get(docKey);
              if(file) {
                this.dispatch(CodeEditorStore, new ActionCodeEditorFileGet(file.projectId, file.key, file.title, file.path, file.type, file.plugin, file.workspaceName));
              }
            }
          }}
        />
      </Tab>
    );
  }
  
  renderTabSearchHeading(data) {
    const hits = 1 !== data.data.hits ? 'hits' : 'hit';
    const files = 1 !== data.data.files ? 'files' : 'file';
    const searchMessage = `Search"${data.search}" <${data.data.hits} ${hits} in ${data.data.files} ${files} of ${data.data.searched} searched>`;
    return (
      <tr key="search_header" className="editor_search_header">
        <td>{searchMessage}</td>
      </tr>
    );
  }
  
  renderTabSearchFileFound(foundFile, found, search, key) {
    const classNameSearchFound = this.state.CodeEditorStore.persistentData.darkMode ? "editor_search_found_dark" : "editor_search_found_light";
    return (
      <React.Fragment key={`found_fragment_${key}`}>
        <tr key={`found_tr_${key}`} className={classNameSearchFound}>
          <td>
            {`Line: ${found.lineNbr} `}
            <a className="editor_search_found"
              onClick={(e) => {
                const url = `./${foundFile.file}`.replace(new RegExp('[/\\\\]', 'g'), '/');
                //this.dispatch(CodeEditorStore, new ActionCodeEditorFileSet(url, `?line=${found.lineNbr}`));
              }}>
              {found.lineBefore}<span className="editor_search_found">{search}</span>{found.lineAfter}
            </a>
          </td>
        </tr>
      </React.Fragment>
    );
  }
  
  renderTabSearchFile(foundFile, search, index) {
    const rows = [];
    foundFile.indexes.forEach((found, fIndex) => {
      rows.push(this.renderTabSearchFileFound(foundFile, found, search, `${index}_${fIndex}`));
    });
    const hits = 1 !== foundFile.indexes.length ? 'hits' : 'hit';
    const fileMessage = `<${foundFile.indexes.length} ${hits}> ${foundFile.file}`;
    return (
      <React.Fragment key={`file_fragment_${index}`}>
        <tr key={`file_tr_${index}`} className={"editor_search_file"}>
          <td>{fileMessage}</td>
        </tr>
        {rows}
      </React.Fragment>
    );
  }
  
  renderTabSearchRows(data) {
    const rows = [];
    rows.push(this.renderTabSearchHeading(data));
    if(0 !== data.data.foundFiles.length) {
      data.data.foundFiles.forEach((foundFile, index) => {
        rows.push(this.renderTabSearchFile(foundFile, data.search, index));
      });
    }
    return (
      <table>
        <tbody>
          {rows}
        </tbody>
      </table>
    );
  }
  
  renderTabHeading(titleHeading, titleHeadingClassName) {
    if(titleHeading) {
      return (
        <span className={titleHeadingClassName}>
          {titleHeading}
        </span>
      );
    }
    else {
      return null;
    }
  }
  
  renderTabSearch(data, key) {
    const rows = this.renderTabSearchRows(data);
    return (
      <Tab eventKey={key} key={key}
        title={
          <div>
            {this.renderTabHeading(data.titleHeading, data.titleHeadingClassName)}
            {data.title}
            <button type="button" className={this.theme(this.state.CodeEditorStore.persistentData.darkMode, 'middle_code_editor_close')} aria-label="Close"
              onClick={(e, a) => {
                this.close = true;
                this.dispatch(CodeEditorStore, new ActionCodeEditorFileClose(key));
              }}
            >
              ×
            </button>
          </div>
        }
        onMove={(fromKey, toKey) => {
          this.dispatch(CodeEditorStore, new ActionCodeEditorFileMove(fromKey, toKey));
        }}
      >
        <div className="editor_search bootstrap_same_size_as_parent">
          {rows}
        </div>
      </Tab>
    );
  }
  
  renderTab(options) {
    const result = [];
    this.state.CodeEditorStore.files.forEach((data, key) => {
      if('search' === data.tabType) {
        result.push(this.renderTabSearch(data, key));
      }
      else {
        result.push(this.renderTabFile(data, key));
      }
    });
    return result;
  }
  
  renderTabs(options) {
    if(0 !== this.state.CodeEditorStore.files.size) {
      return (
        <Tabs id="code_editor_tabs" /*ref={'code_editor_tabs'}*/ draggable="true" activeKey={null !== this.state.CodeEditorStore.current.file ? this.state.CodeEditorStore.current.file.key : null} darkMode={this.state.CodeEditorStore.persistentData.darkMode}
          onSelect={(selectedKey) => {
            if(!this.close) {
              const file = this.state.CodeEditorStore.files.get(selectedKey);
              if('search' === file.tabType) {
                this.dispatch(CodeEditorStore, new ActionCodeEditorWorkspaceSearchSet(file.key));
              }
              else {
                this.dispatch(CodeEditorStore, new ActionCodeEditorFileGet(file.projectId, file.key, file.title, file.path, file.type, file.plugin, file.workspaceName));
              }
            }
            else {
              this.close = false;
            }
          }}
        >
          {this.renderTab(options)}
        </Tabs>
      );
    }
    else {
      return null;
    }
  }
  
  getOptions(type) {
    return {
      highlightActiveLine: true,
      closeBrackets: true,
      bracketMatching: true,
      drawSelection: true,
      history: true,
      autocompletion: true,
      
      tabKeyAction: 'indent', 
      tabSize: 2,
      indentType: 'spaces',
      indentUnit: 2,
      
      darkMode: true,
      lineNumbers: true,
      highlightActiveLineGutter: true,
      breakpointGutter: false,
      foldGutter: true,
      emptyLine: true,
      linesQuery: true,
      
      type: type
    };
  }
  
  render () {
    const darkMode = this.state.CodeEditorStore.persistentData.darkMode;
    return (
      <div className={this.theme(darkMode, "view")}>
        {this.renderTabs()}
      </div>
    );
  }
}


// Double code /App layer log log-type
MiddleCodeEditorView.csses = [
  'log_engine',
  'log_debug',
  'log_error',
  'log_warning',
  'log_ip',
  'log_gui',
  'log_verify_success',
  'log_verify_failure',
  'log_test_data',
  'log_browser_log',
  'log_browser_err'
];


MiddleCodeEditorView.contextType = RouterContext;
MiddleCodeEditorView.REMOVE_KEY_LENGTH = 'code_editor_tabs-tab-'.length;
