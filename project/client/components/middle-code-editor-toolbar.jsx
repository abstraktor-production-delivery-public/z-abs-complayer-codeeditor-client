
'use strict';

import CodeEditorStore from '../stores/code-editor-store';
import { ActionCodeEditorWorkspaceNew, ActionCodeEditorWorkspaceGet, ActionCodeEditorWorkspaceSearch } from '../actions/action-code-editor-workspace';
import { ActionCodeEditorProjectUpdate, ActionCodeEditorProjectUpdateAll } from '../actions/action-code-editor-project';
import { ActionCodeEditorPersistentDataUpdate } from '../actions/action-code-editor-persistent-data';
import { ActionCodeEditorFolderNew, ActionCodeEditorFolderAdd, ActionCodeEditorFolderUpdate, ActionCodeEditorFolderRemove, ActionCodeEditorFolderDelete } from '../actions/action-code-editor-folder';
import { ActionCodeEditorFileNew, ActionCodeEditorFileAdd, ActionCodeEditorFileRename, ActionCodeEditorFileUpdate, ActionCodeEditorFileUpdateAll, ActionCodeEditorFileRemove, ActionCodeEditorFileDelete } from '../actions/action-code-editor-file';
import ModalDialogStore from 'z-abs-complayer-modaldialog-client/client/stores/modal-dialog-store';
import { ActionModalDialogShow, ActionModalDialogHide, ActionModalDialogPending, ActionModalDialogResult } from 'z-abs-complayer-modaldialog-client/client/actions/action-modal-dialog';
import ModalDialogWorkspaceNew from 'z-abs-complayer-modaldialog-client/client/modal-dialog-workspace-new';
import ModalDialogWorkspaceOpen from 'z-abs-complayer-modaldialog-client/client/modal-dialog-workspace-open';
import ModalDialogFolderAdd from 'z-abs-complayer-modaldialog-client/client/modal-dialog-folder-add';
import ModalDialogFolderNew from 'z-abs-complayer-modaldialog-client/client/modal-dialog-folder-new';
import ModalDialogFolderProperties from 'z-abs-complayer-modaldialog-client/client/modal-dialog-folder-properties';
import ModalDialogFolderRemove from 'z-abs-complayer-modaldialog-client/client/modal-dialog-folder-remove';
import ModalDialogFileAdd from 'z-abs-complayer-modaldialog-client/client/modal-dialog-file-add';
import ModalDialogFileNew from 'z-abs-complayer-modaldialog-client/client/modal-dialog-file-new';
import ModalDialogFileProperties from 'z-abs-complayer-modaldialog-client/client/modal-dialog-file-properties';
import ModalDialogFileRemove from 'z-abs-complayer-modaldialog-client/client/modal-dialog-file-remove';
import ModalDialogSearch from 'z-abs-complayer-modaldialog-client/client/modal-dialog-search';
import Button from 'z-abs-complayer-bootstrap-client/client/button';
import { ActionDialogFileGet, ActionDialogFileSet, ActionDialogWorkspaceSet } from 'z-abs-corelayer-client/client/actions/action-dialog-file';
import DialogFileStore from 'z-abs-corelayer-client/client/stores/dialog-file-store';
import {RouterContext} from 'z-abs-complayer-router-client/client/react-component/router-context';
import ReactComponentRealtime from 'z-abs-corelayer-client/client/react-component/react-component-realtime';
import React from 'react';


export default class MiddleCodeEditorToolbar extends ReactComponentRealtime {
  constructor(props) {
    super(props, [CodeEditorStore, ModalDialogStore, DialogFileStore]);
    this._modalDialogWorkspaceNew = null;
    this.boundKeyDown = this._keyDown.bind(this);
    this.buttonFileSaveDisabled = true;
    this.buttonFileSaveAllDisabled = true;
  }
  
  didMount() {
    window.addEventListener('keydown', this.boundKeyDown, true);
  }
  
  shouldUpdate(nextProps, nextState) {
    return !this.shallowCompare(this.state.CodeEditorStore.project, nextState.CodeEditorStore.project)
      || !this.shallowCompareMapValues(this.state.CodeEditorStore.projects, nextState.CodeEditorStore.projects)
      || !this.shallowCompare(this.state.CodeEditorStore.current, nextState.CodeEditorStore.current)
      || !this.shallowCompareMapValues(this.state.CodeEditorStore.files, nextState.CodeEditorStore.files)
      || !this.shallowCompare(this.state.CodeEditorStore.persistentData, nextState.CodeEditorStore.persistentData)
      || !this.shallowCompareMapValues(this.state.ModalDialogStore.modalResults, nextState.ModalDialogStore.modalResults)
      || !this.shallowCompare(this.state.DialogFileStore.project, nextState.DialogFileStore.project)
      || !this.shallowCompare(this.state.DialogFileStore.current, nextState.DialogFileStore.current)
      || !this.shallowCompare(this.state.DialogFileStore.original, nextState.DialogFileStore.original)
      || !this.shallowCompare(this.state.DialogFileStore.workspace, nextState.DialogFileStore.workspace)
      || !this.shallowCompare(this.state.DialogFileStore.workspaces, nextState.DialogFileStore.workspaces);
  }
  
  willUnmount() {
    window.removeEventListener('keydown', this.boundKeyDown, true);
  }
  
  _fileSave() {
    this.dispatch(CodeEditorStore, new ActionCodeEditorFileUpdate(this.state.CodeEditorStore.current.file.key));
  }
  
  _fileSaveAll() {
    const keys = [];
    this.state.CodeEditorStore.files.forEach((file) => { 
      if(file.codeChanged) {
        keys.push(file.key);
      }
    });
    this.dispatch(CodeEditorStore, new ActionCodeEditorFileUpdateAll(keys));
  }
  
  _keyDown(e) {
    if(e.ctrlKey && e.shiftKey && 'S' === e.key) {
      if(!this.buttonFileSaveAllDisabled) {
        e.preventDefault();
        this._fileSaveAll();
      }
    }
    if(e.ctrlKey && 's' === e.key) {
      if(!this.buttonFileSaveDisabled) {
        e.preventDefault();
        this._fileSave();
      }
    }
  }
  
  renderButtonNewWorkspace() {
    let disabled = true;
    this.state.CodeEditorStore.files.forEach((file) => { 
      disabled = disabled && !file.codeChanged;
    });
    disabled = !disabled || !this.state.CodeEditorStore.project.isSaved();
    return (
      <Button placement="bottom" heading="New" content="Workspace" disabled={disabled}
        onClick={(e) => {
          this._modalDialogWorkspaceNew.show(this.state.CodeEditorStore.current.folder);
        }}
      >
        <span className="glyphicon glyphicon_workspace glyphicon-briefcase" aria-hidden="true"></span>
      </Button>
    );
  }
  
  renderButtonOpenWorkspace() {
    return (
      <Button placement="bottom" heading="Open" content="Workspace"
        onClick={(e) => {
          const dir = '../../..';
          this.dispatch(DialogFileStore, new ActionDialogFileGet(null, null, null, null, 'workspace'));
          this.dispatch(ModalDialogStore, new ActionModalDialogShow('Code_Workspace_Open'));
        }}
      >
        <span className="glyphicon glyphicon_workspace glyphicon-briefcase" aria-hidden="true"></span>
        <span className="glyphicon glyphicon_workspace glyphicon-circle-arrow-up" aria-hidden="true" style={{top: 6, left: -3, width: 0, transform: 'scale(0.8)'}}></span>
      </Button>
    );
  }
    
  _areAllProjectsSaved() {
    let isProjectsSaved = true;
    const projectIds = [];
    this.state.CodeEditorStore.projects.forEach((project) => {
      isProjectsSaved = isProjectsSaved && project.isSaved();
      if(!project.isSaved()) {
        projectIds.push(project.projectId);
      }
    });
    if(!this.state.CodeEditorStore.project.isSaved()) {
      projectIds.push(this.state.CodeEditorStore.project.projectId);
    }
    return {
      saved: 0 === projectIds.length,
      projectIds: projectIds
    };
  }
  
  renderButtonSaveAllProjects() {
    const allProjectsSaved = this._areAllProjectsSaved();
    const disabled = allProjectsSaved.saved;
    return (
      <Button placement="bottom" heading="Save All" content="Project" disabled={disabled}
        onClick={(e) => {
          this.dispatch(CodeEditorStore, new ActionCodeEditorProjectUpdateAll(allProjectsSaved.projectIds));
        }}
      >
        <span className="glyphicon glyphicon_project glyphicon-credit-card" aria-hidden="true"></span>
        <span className="glyphicon glyphicon_project glyphicon-circle-arrow-down" aria-hidden="true" style={{top: 6, left: -3, width: 0, transform: 'scale(0.8)'}}></span>
      </Button>
    );
  }
  
  renderButtonFolderNew() {
    const current = this.state.CodeEditorStore.current;
    const enabled = 'folder' === current.type && 'project_folder' !== current.folder.data.type;
    return (
      <Button placement="bottom" heading="New" content="Folder" disabled={!enabled}
        onClick={(e) => {
          this.dispatch(ModalDialogStore, new ActionModalDialogShow('Code_Folder_New'));
        }}
      >
        <span className="glyphicon glyphicon-folder-close" aria-hidden="true"></span>
      </Button>
    );
  }
  
  renderButtonFolderAdd() {
    const current = this.state.CodeEditorStore.current;
    const enabled = 'folder' === current.type && 'project_folder' !== current.folder.data.type;
    const workspaceName = this.state.CodeEditorStore.current.query?.workspace;
    return (
      <Button placement="bottom" heading="Add" content="Folder" disabled={!enabled}
        onClick={(e) => {
          const folder = current.folder;
          if(this.state.CodeEditorStore.project.projectId === folder.projectId) {
            const dir = `${folder.data.path}/${folder.title}`;
            const project =  this.state.CodeEditorStore.project;
            this.dispatch(DialogFileStore, new ActionDialogFileGet(dir, project.getRootName(), [dir], project.getFileNames(dir), 'folder', null, false, workspaceName));
          }
          else {
            const dir = `${folder.data.path}/${folder.title}`;
            const project =  this.state.CodeEditorStore.projects.get(folder.projectId);
            this.dispatch(DialogFileStore, new ActionDialogFileGet(dir, project.getRootName(), [dir], project.getFileNames(dir), 'folder', project.type, project.plugin, workspaceName));
          }
          this.dispatch(ModalDialogStore, new ActionModalDialogShow('Code_Folder_Add'));
        }}
      >
        <span className="glyphicon glyphicon-folder-close" aria-hidden="true"></span>
        <span className="glyphicon glyphicon-circle-arrow-up" aria-hidden="true" style={{top: 6, left: -3, width: 0, transform: 'scale(0.8)'}}></span>
      </Button>
    );
  }
  
  renderButtonFolderProperties() {
    const current = this.state.CodeEditorStore.current;
    const enabled = 'folder' === current.type && 'project_folder' !== current.folder.data.type && 'static_folder' !== current.folder.data.type;
    return (
      <Button placement="bottom" heading="Properties" content="Folder" disabled={!enabled}
        onClick={(e) => {
          this.dispatch(ModalDialogStore, new ActionModalDialogShow('Code_Folder_Properties'));
        }}
      >
        <span className="glyphicon glyphicon-folder-close" aria-hidden="true"></span>
        <span className="glyphicon glyphicon-wrench" aria-hidden="true" style={{top: 6, left: -3, width: 0, transform: 'scale(0.8)'}}></span>
      </Button>
    );
  }
  
  renderButtonFolderDelete() {
    const allProjectsSaved = this._areAllProjectsSaved();
    const current = this.state.CodeEditorStore.current;
    const enabledFolder = current && 'folder' === current.type && 'static_folder' !== current.folder.data.type && 'project_folder' !== current.folder.data.type;
    const disabled = !enabledFolder || !allProjectsSaved.saved;
    return (
      <Button placement="bottom" heading="Remove or Delete" content="Folder" disabled={disabled}
        onClick={(e) => {
          this.dispatch(ModalDialogStore, new ActionModalDialogShow('Code_Folder_Remove'));
        }}
      >
        <span className="glyphicon glyphicon-trash" aria-hidden="true" style={{left: 3}}></span>
        <span className="glyphicon glyphicon-folder-close" aria-hidden="true" style={{left: -17, width: 0, transform: 'scale(0.6)'}}></span>
      </Button>
    );
  }
  
  renderButtonFileNew() {
    const current = this.state.CodeEditorStore.current;
    const enabled = 'folder' === current.type && 'project_folder' !== current.folder.data.type;
    return (
      <Button placement="bottom" heading="New" content="File" disabled={!enabled}
        onClick={(e) => {
          this.dispatch(ModalDialogStore, new ActionModalDialogShow('Code_File_New'));
        }}
      >
        <span className="glyphicon glyphicon-file" aria-hidden="true"></span>
      </Button>
    );
  }
  
  renderButtonFileAdd() {
    const current = this.state.CodeEditorStore.current;
    const enabled = 'folder' === current.type && 'project_folder' !== current.folder.data.type;
    const workspaceName = this.state.CodeEditorStore.current.query?.workspace;
    return (
      <Button placement="bottom" heading="Add" content="File" disabled={!enabled}
        onClick={(e) => {
          const folder = current.folder;
          if(this.state.CodeEditorStore.project.projectId === folder.projectId) {
            const dir = `${folder.data.path}/${folder.title}`;
            const project = this.state.CodeEditorStore.project;
            this.dispatch(DialogFileStore, new ActionDialogFileGet(dir, project.getRootName(), [dir], project.getFileNames(dir), 'file', null, false, workspaceName));
          }
          else {
            const dir = `${folder.data.path}/${folder.title}`;
            const project =  this.state.CodeEditorStore.projects.get(folder.projectId);
            this.dispatch(DialogFileStore, new ActionDialogFileGet(dir, project.getRootName(), [dir], project.getFileNames(dir), 'file',  project.type, project.plugin, workspaceName));
          }
          this.dispatch(ModalDialogStore, new ActionModalDialogShow('Code_File_Add'));
        }}
      >
        <span className="glyphicon glyphicon-file" aria-hidden="true"></span>
        <span className="glyphicon glyphicon-circle-arrow-up" aria-hidden="true" style={{top: 6, left: -3, width: 0, transform: 'scale(0.8)'}}></span>
      </Button>
    );
  }
  
  renderButtonFileSave() {
    this.buttonFileSaveDisabled = 'file' !== this.state.CodeEditorStore.current.type || !this.state.CodeEditorStore.current.file.codeChanged;
    return (
      <Button placement="bottom" heading="Save" content="File" shortcut="Ctrl+S" disabled={this.buttonFileSaveDisabled}
        onClick={(e) => {
          this._fileSave();
        }}
      >
        <span className="glyphicon glyphicon-file" aria-hidden="true"></span>
        <span className="glyphicon glyphicon-circle-arrow-down" aria-hidden="true" style={{top: 6, left: -3, width: 0, transform: 'scale(0.8)'}}></span>
      </Button>
    );
  }
  
  renderButtonFileSaveAll() {
    this.buttonFileSaveAllDisabled = true;
    this.state.CodeEditorStore.files.forEach((file) => { 
      this.buttonFileSaveAllDisabled = this.buttonFileSaveAllDisabled && !file.codeChanged;
    });
    return (
      <Button placement="bottom" heading="Save" content="All Files" shortcut="Ctrl+Shift+S" disabled={this.buttonFileSaveAllDisabled}
        onClick={(e) => {
          this._fileSaveAll();
        }}
      >
        <span className="glyphicon glyphicon-file" aria-hidden="true" style={{top: -1, left: -4, transform: 'scale(0.9)'}}></span>
        <span className="glyphicon glyphicon-file" aria-hidden="true" style={{top: 3, left: -10, width: 0, transform: 'scale(0.9)'}}></span>
        <span className="glyphicon glyphicon-circle-arrow-down" aria-hidden="true" style={{top: 6, left: -3, width: 0, transform: 'scale(0.8)'}}></span>
      </Button>
    );
  }
  
  renderButtonFileProperties() {
    return (
      <Button id="code_file_delete" placement="bottom" heading="Properties" content="File" disabled={'file' !== this.state.CodeEditorStore.current.type}
        onClick={(e) => {
          this.dispatch(ModalDialogStore, new ActionModalDialogShow('Code_File_Properties'));
        }}
      >
        <span className="glyphicon glyphicon-file" aria-hidden="true"></span>
        <span className="glyphicon glyphicon-wrench" aria-hidden="true" style={{top:'6px',left:'-4px',width:'0px'}}></span>
      </Button>
    );
  }
  
  renderButtonFileDelete() {
    const allProjectsSaved = this._areAllProjectsSaved();
    const disabled = 'file' !== this.state.CodeEditorStore.current.type && 'invalid_file' !== this.state.CodeEditorStore.current.type || !allProjectsSaved.saved;
    return (
      <Button placement="bottom" heading="Remove or Delete" content="File" disabled={disabled}
        onClick={(e) => {
          this.dispatch(ModalDialogStore, new ActionModalDialogShow('Code_File_Remove'));
        }}
      >
        <span className="glyphicon glyphicon-trash" aria-hidden="true" style={{left: 3}}></span>
        <span className="glyphicon glyphicon-file" aria-hidden="true" style={{left: -17, width: 0, transform: 'scale(0.6)'}}></span>
      </Button>
    );
  }
  
  renderButtonInFileSearch() {
    return (
      <Button placement="bottom" heading="Search" content="In files" 
        onClick={(e) => {
          this._modalDialogSearch.show();
        }}
      >
        <span className="glyphicon glyphicon-file" aria-hidden="true"></span>
        <span className="glyphicon glyphicon-search" aria-hidden="true" style={{top:'0px',left:'4px',width:'0px',transform:'scale(0.9) rotate(90deg)'}}></span>
      </Button>
    );
  }
  
  renderButtonDarkMode() {
    const darkMode = this.state.CodeEditorStore.persistentData.darkMode;
    return (
      <Button active={darkMode} placement="bottom" heading="View" content="Dark mode"
        onClick={(e) => {
          this.dispatch(CodeEditorStore, new ActionCodeEditorPersistentDataUpdate('', 'darkMode', !darkMode));
        }}
      >
        <span className="glyphicon glyphicon-certificate" aria-hidden="true"></span>
      </Button>
    );
  }
  
  renderButtonEmptyLine() {
    const emptyLine = this.state.CodeEditorStore.persistentData.emptyLine;
    return (
      <Button active={emptyLine} placement="bottom" heading="View" content="Empty Line"
        onClick={(e) => {
          this.dispatch(CodeEditorStore, new ActionCodeEditorPersistentDataUpdate('', 'emptyLine', !emptyLine));
        }}
      >
        _
      </Button>
    );
  }
  
  renderModalDialogWorkspaceNew() {
    return (
      <ModalDialogWorkspaceNew ref={(c) => this._modalDialogWorkspaceNew = c} heading="New Workspace" result={'result'}
        onWorkspaceNew={(appName, appType) => {
          this.dispatch(CodeEditorStore, new ActionCodeEditorWorkspaceNew(appName, appType));
        }}
        onClear={() => {}}
      />
    );
  }
  
  renderModalDialogWorkspaceOpen() {
    const name = 'Code_Workspace_Open';
    return (
      <ModalDialogWorkspaceOpen name={name} heading="Open Workspace" workspaces={this.state.DialogFileStore.workspaces} modalResults={this.state.ModalDialogStore.modalResults}
        onAdd={() => {
          this.dispatch(ModalDialogStore, new ActionModalDialogPending(name));
          this.dispatch(CodeEditorStore, new ActionCodeEditorWorkspaceGet(this.state.DialogFileStore.workspace, this.state.DialogFileStore.workspace));
          this.dispatch(ModalDialogStore, new ActionModalDialogResult(name, {
            code: 'success',
            error: null,
            msg: ''
          }));
          this.props.onOpen(this.state.DialogFileStore.workspace);
        }}
        onChoose={(path) => {
          this.dispatch(DialogFileStore, new ActionDialogWorkspaceSet(path));
        }}
        onHide={() => {
          this.dispatch(ModalDialogStore, new ActionModalDialogHide(name));
        }}
      />
    );
  }
  
  renderModalDialogFolderNew() {
    const name = 'Code_Folder_New';
    return (
      <ModalDialogFolderNew name={name} folder={this.state.CodeEditorStore.current.folder} heading="New Folder" modalResults={this.state.ModalDialogStore.modalResults}
        onFolderNew={(projectId, title, data) => {
          this.dispatch(ModalDialogStore, new ActionModalDialogPending(name));
          this.dispatch(CodeEditorStore, new ActionCodeEditorFolderNew(projectId, title, data), (response) => {
            this.dispatch(ModalDialogStore, new ActionModalDialogResult(name, response.result));
          });
        }}
        onHide={() => {
          this.dispatch(ModalDialogStore, new ActionModalDialogHide(name));
        }}
      />
    );
  }
  
  renderModalDialogFolderAdd() {
    const name = 'Code_Folder_Add';
    const folder = this.state.CodeEditorStore.current.folder;
    const projectId = folder !== null ? folder.projectId : null;
    return (
      <ModalDialogFolderAdd name={name} heading="Add Folder" folder={this.state.CodeEditorStore.current.folder} project={this.state.DialogFileStore.project} current={this.state.DialogFileStore.current} original={this.state.DialogFileStore.original} modalResults={this.state.ModalDialogStore.modalResults}
        onAdd={(projectId, title, data) => {
          this.dispatch(ModalDialogStore, new ActionModalDialogPending(name));
          this.dispatch(CodeEditorStore, new ActionCodeEditorFolderAdd(projectId, title, data));
          this.dispatch(ModalDialogStore, new ActionModalDialogResult(name, {
            code: 'success',
            error: null,
            msg: ''
          }));
        }}
        onChoose={(path) => {
          this.dispatch(DialogFileStore, new ActionDialogFileSet(path));
        }}
        onHide={() => {
          this.dispatch(ModalDialogStore, new ActionModalDialogHide(name));
        }}
      />
    );
  }
  
  renderModalDialogFolderProperties() {
    const name = 'Code_Folder_Properties';
    const workspaceName = this.state.CodeEditorStore.current.query?.workspace;
    return (
      <ModalDialogFolderProperties name={name} folder={this.state.CodeEditorStore.current.folder} heading="Folder Properties" modalResults={this.state.ModalDialogStore.modalResults}
        onFolderProperties={(projectId, title, newTitle, data) => {
          this.dispatch(ModalDialogStore, new ActionModalDialogPending(name));
          this.dispatch(CodeEditorStore, new ActionCodeEditorFolderUpdate(projectId, title, newTitle, data, workspaceName), (response) => {
            this.dispatch(ModalDialogStore, new ActionModalDialogResult(name, response.result));
          });
        }}
        onHide={() => {
          this.dispatch(ModalDialogStore, new ActionModalDialogHide(name));
        }}
      />
    );
  }
  
  renderModalDialogFolderRemove() {
    const name = 'Code_Folder_Remove';
    const workspaceName = this.state.CodeEditorStore.current.query?.workspace;
    return (
      <ModalDialogFolderRemove name={name} folder={this.state.CodeEditorStore.current.folder} heading="Remove or Delete Folder" modalResults={this.state.ModalDialogStore.modalResults}
        onFolderRemove={(projectId, title, key, data) => {
          this.dispatch(ModalDialogStore, new ActionModalDialogPending(name));
          this.dispatch(CodeEditorStore, new ActionCodeEditorFolderRemove(projectId, title, key, data));
          this.dispatch(ModalDialogStore, new ActionModalDialogResult(name, {
            code: 'success',
            error: null,
            msg: ''
          }));
        }}
        onFolderDelete={(projectId, title, key, data) => {
          this.dispatch(ModalDialogStore, new ActionModalDialogPending(name));
          this.dispatch(CodeEditorStore, new ActionCodeEditorFolderDelete(projectId, title, key, data, workspaceName), (response) => {
            this.dispatch(ModalDialogStore, new ActionModalDialogResult(name, response.result));
          });
        }}
        onHide={() => {
          this.dispatch(ModalDialogStore, new ActionModalDialogHide(name));
        }}
      />
    );
  }
  
  renderModalDialogFileNew() {
    const name = 'Code_File_New';
    const workspaceName = this.state.CodeEditorStore.current.query?.workspace;
    return (
      <ModalDialogFileNew name={name} heading="New Code File" folder={this.state.CodeEditorStore.current.folder} templateNames={MiddleCodeEditorToolbar.TEMPLATE_NAMES} modalResults={this.state.ModalDialogStore.modalResults}
        onFileNew={(projectId, path, title, type, templateName) => {
          this.dispatch(ModalDialogStore, new ActionModalDialogPending(name));
          this.dispatch(CodeEditorStore, new ActionCodeEditorFileNew(projectId, path, title, type, templateName, null, workspaceName), (response) => {
            this.dispatch(ModalDialogStore, new ActionModalDialogResult(name, response.result));
          });
        }}
        onHide={() => {
          this.dispatch(ModalDialogStore, new ActionModalDialogHide(name));
        }}
      />
    );
  }
  
  renderModalDialogFileAdd() {
    const name = 'Code_File_Add';
    const folder = this.state.CodeEditorStore.current.folder;
    const projectId = folder !== null ? folder.projectId : null;
    return (
      <ModalDialogFileAdd name={name} heading="Add Code File" folder={this.state.CodeEditorStore.current.folder} project={this.state.DialogFileStore.project} current={this.state.DialogFileStore.current} original={this.state.DialogFileStore.original} modalResults={this.state.ModalDialogStore.modalResults}
        onAdd={(projectId, title, path, type) => {
          this.dispatch(ModalDialogStore, new ActionModalDialogPending(name));
          this.dispatch(CodeEditorStore, new ActionCodeEditorFileAdd(projectId, title, path, type));
          this.dispatch(ModalDialogStore, new ActionModalDialogResult(name, {
            code: 'success',
            error: null,
            msg: ''
          }));
        }}
        onChoose={(path) => {
          this.dispatch(DialogFileStore, new ActionDialogFileSet(path));
        }}
        onHide={() => {
          this.dispatch(ModalDialogStore, new ActionModalDialogHide(name));
        }}
      />
    );
  }
  
  renderModalDialogFileProperties() {
    const name = 'Code_File_Properties';
    const workspaceName = this.state.CodeEditorStore.current.query?.workspace;
    return (
      <ModalDialogFileProperties name={name} heading="File Properties" file={this.state.CodeEditorStore.current.file} modalResults={this.state.ModalDialogStore.modalResults}
        onFileProperties={(projectId, path, title, newTitle) => {
          this.dispatch(ModalDialogStore, new ActionModalDialogPending(name));
          this.dispatch(CodeEditorStore, new ActionCodeEditorFileRename(projectId, path, title, newTitle, workspaceName), (response) => {
            this.dispatch(ModalDialogStore, new ActionModalDialogResult(name, response.result));
          });
        }}
        onHide={() => {
          this.dispatch(ModalDialogStore, new ActionModalDialogHide(name));
        }}
      />
    );
  }
  
  renderModalDialogFileRemove() {
    const name = 'Code_File_Remove';
    const workspaceName = this.state.CodeEditorStore.current.query?.workspace;
    return (
      <ModalDialogFileRemove name={name} file={this.state.CodeEditorStore.current.file} doDelete={'invalid_file' !== this.state.CodeEditorStore.current.type} heading="Remove or Delete Code File" modalResults={this.state.ModalDialogStore.modalResults}
        onFileRemove={(projectId, key) => {
          this.dispatch(ModalDialogStore, new ActionModalDialogPending(name));
          this.dispatch(CodeEditorStore, new ActionCodeEditorFileRemove(projectId, key));
          this.dispatch(ModalDialogStore, new ActionModalDialogResult(name, {
            code: 'success',
            error: null,
            msg: ''
          }));
        }}
        onFileDelete={(projectId, key, plugin) => {
          this.dispatch(ModalDialogStore, new ActionModalDialogPending(name));
          this.dispatch(CodeEditorStore, new ActionCodeEditorFileDelete(projectId, key, plugin, workspaceName), (response) => {
            this.dispatch(ModalDialogStore, new ActionModalDialogResult(name, response.result));
          });
        }}
        onHide={() => {
          this.dispatch(ModalDialogStore, new ActionModalDialogHide(name));
        }}
      />
    );
  }
  
  renderModalDialogSearch() {
    return (
      <ModalDialogSearch ref={(c) => this._modalDialogSearch = c} result={'result'}
        onSearch={(search) => {
          this.dispatch(CodeEditorStore, new ActionCodeEditorWorkspaceSearch(search));
        }}
        onClear={() => {}}
      />
    );
  }
  
  renderWorkspaceModals() {
    if(this.props.showWorkspace) {
      return (
        <>
          {this.renderModalDialogWorkspaceNew()}
          {this.renderModalDialogWorkspaceOpen()}
        </>
      );
    }
    else {
      return null;
    }
  }
  
  renderWorkspaceButtons() {
    if(this.props.showWorkspace) {
      return (
        <>
          <div className="btn-group btn-group-sm" role="group" aria-label="...">
            {this.renderButtonNewWorkspace()}
            {this.renderButtonOpenWorkspace()}
          </div>
        </>
      );
    }
    else {
      return null;
    }
  }
  
  render() {
    return (
      <div className="middle_toolbar middle_code_editor_toolbar">
        {this.renderWorkspaceModals()}
        {this.renderModalDialogFolderNew()}
        {this.renderModalDialogFolderAdd()}
        {this.renderModalDialogFolderProperties()}
        {this.renderModalDialogFolderRemove()}
        {this.renderModalDialogFileNew()}
        {this.renderModalDialogFileAdd()}
        {this.renderModalDialogFileProperties()}
        {this.renderModalDialogFileRemove()}
        {this.renderModalDialogSearch()}
        <div className="btn-toolbar" role="toolbar" aria-label="...">
          {this.renderWorkspaceButtons()}
          <div className="btn-group btn-group-sm" role="group" aria-label="...">
            {this.renderButtonSaveAllProjects()}
          </div>
          <div className="btn-group btn-group-sm" role="group" aria-label="...">
            {this.renderButtonFolderNew()}
            {this.renderButtonFolderAdd()}
            {this.renderButtonFolderProperties()}
            {this.renderButtonFolderDelete()}
          </div>
          <div className="btn-group btn-group-sm" role="group" aria-label="...">
            {this.renderButtonFileNew()}
            {this.renderButtonFileAdd()}
            {this.renderButtonFileSave()}
            {this.renderButtonFileSaveAll()}
            {this.renderButtonFileProperties()}
            {this.renderButtonFileDelete()}
          </div>
          <div className="btn-group btn-group-sm" role="group" aria-label="...">
            {this.renderButtonInFileSearch()}
          </div>
          <div className="btn-group btn-group-sm pull-right" role="group" aria-label="...">
            {this.renderButtonEmptyLine()}
            {this.renderButtonDarkMode()}
          </div>
        </div>
      </div>
    );
  }
}


MiddleCodeEditorToolbar.TEMPLATE_NAMES = [
  {name: 'Class [class js file]', type:'js', icon: ''},
  {name: 'None [empty js file]', type:'js', icon: ''},
  {name: 'None [empty jsx file]', type:'jsx', icon: ''},
  {name: 'None [empty css file]', type:'css', icon: ''},
  {name: 'None [empty html file]', type:'html', icon: ''},
  {name: 'React Base', type:'jsx', icon: ''},
  {name: 'React Store', type:'jsx', icon: ''},
  {name: 'React Realtime', type:'jsx', icon: ''}
];

MiddleCodeEditorToolbar.contextType = RouterContext;

